# ytskip

**ytskip** is a Chrome [content script extension] that automatically hides
ads and clicks skip buttons on YouTube pages.

[content script extension]: https://developer.chrome.com/extensions/content_scripts
